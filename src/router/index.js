import Vue from 'vue'
import Router from 'vue-router'
// import Hello from '@/components/Hello'
import Search from '@/pages/SearchPage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Search',
      component: Search
    }
  ]
})
