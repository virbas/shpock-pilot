// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
/* import { VueStyle } from 'vue-style' */

Vue.config.productionTip = false

/* Vue.use(VueStyle, {
  defaults: {
    'primary-bg': 'green',
    'font-size': '50px'
  }
}) */

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
